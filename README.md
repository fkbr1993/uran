# Установка

### Шаг 1 ###

   1) Склонировать Uran
   `git clone git@bitbucket.org:fkbr1993/uran.git`

   2) Запустить композер

```
composer global require "fxp/composer-asset-plugin:~1.0.0"
composer install
```

   3) Инициализировать проект `./init`, для локальной разработки выбираем вариант 0 - Development, для продакшна 1 - Production

   4) В основных конфигурационных файлах не указано подключение к базе данных, это нужно сделать в файле `common/config/main-local.php`

```
	<?php
    return [
		'components' => [
	             'db' => [
	                 'class' => 'yii\db\Connection',
	                 'dsn' => 'mysql:host=localhost;dbname=databasename',
	                 'username' => 'user',
	                 'password' => 'passwd',
	                 'charset' => 'utf8',
	             ]
	    ],
	];
```

   если необходимо на локальной машине использовать какой то другой компонент или модуль,
   нужно переопределить это в файлах `main-local.php` ( или `params-local.php` для переопределения настроек )
   в папках конфигурации каждого из приложений эти файлы не попадают в репозиторий и их изменения не повлияют на работу других
   окружений (  дев-сервера и продакшен-сервера )

### Шаг 3 (для локальной машины) ###

   5) в файле `bootstrap/configure-ubuntu-trusty` прописать переменные DB_NAME, DB_USER, DB_PASSWORD
   Значения переменных должны совпадать с значениями, которые указаны в файле `common/config/main-local.php`

   6) запускаем Vagrant в корневой папке проекта `vagrant up` или из phpstorm ( если вы поднимаете эту виртуальную машину в первый раз,
   то нужен будет интернет )

### Шаг 3 (для продакшна) ###

   5) запускаем миграции ./yii migrate

# Настройка

## Настройка доменов

Для работы приложения необходимы 2 домена: для сайта и его административной части.
2 домена должны ссылаться на `/frontend/web` и `/backend/web` соответственно.
(Например `example.com` и `admin.example.com`)

## Настройка входа в админку

логин и пароль на админку задаются в миграции `/console/migrations/mm130524_201442_init.php`

Работа с базой ТОЛЬКО через миграции. все данные, которые попадают в базу тоже должны быть в миграциях.
Можно делать дампы и вызывать их, но не копаться в структуре и данных руками.

Пример создания таблицы:

```
<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Create payment table
 * Class m150406_105201_create_payment_table
 */
class m150406_105201_create_payment_table extends Migration
{
    public function up()
    {

	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
		    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    }

	    $this->createTable('{{%payment}}', [
		    'id' => Schema::TYPE_PK,
		    'credit_id' => Schema::TYPE_STRING . ' NOT NULL',
		    'value' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'email' => Schema::TYPE_STRING . '(30)',
		    'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
		    'created_at' => Schema::TYPE_INTEGER . ' DEFAULT CURRENT_TIMESTAMP',
		    'updated_at' => Schema::TYPE_INTEGER,
	    ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%payment}}');
    }
}


```

ОБЯЗАТЕЛЬНО использовать связи! прописывать их отдельно (в отдельной миграции), не забывать про них. пример:

```
<?php

use yii\db\Migration;

/**
 * Class m150323_091707_credit_add_fk_to_shop
 * Create foreign key from credit to shop
 */
class m150323_091707_credit_add_fk_to_shop extends Migration
{
	public function up()
	{
		$this->addForeignKey(
			'credit_to_shop',
			'credit',
			['shop_id', ],
			'shop',
			['id',],
			'RESTRICT',
			'RESTRICT'
		);
	}

	public function down()
	{
		$this->dropForeignKey('credit_to_shop', 'credit');
	}
}

```


--------------------------------------
# Требования к оформлению и именованию

* весь sql код писать КАПСОМ

* для числовых полей, которые не могут быть отрицательными (индексы, каунтеры) должны быть UNSIGNED

* indexes in the last rows

* устанавливать движок и кодировку: `ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci`

* поля по умолчанию (могут отсутствовать некоторые поля в зависимости от потребностей таблицы):

	1. `published` - опубликованность записи, если не опубликована, то ее нельзя увидеть нигде

	2. `visible` - видимость записи, то есть она может быть опубликована и не видна в списках, но доступна по ссылке

	3. `position` - сортировка записей

	4. `created` - `time()` создания записи

	5. `modified` - `time()` изменения записи

* именование полей по умолчанию:

	1. `label` для заголовка объекта

	2. `alias` для ссылки объекта

	3. `content` для контентной части объекта

	4. `announce` для короткого описания объекта

# Создание модуля

Модули создаём через gii. Прописываем его в конфиге (aliases и modules)

```
'aliases' => array(
	...
	'moduleName' => realpath(
		__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'moduleName'
	),
	...
),
'modules' => array(
	...
	'moduleName' => array(
		'class' => '\moduleName\ModuleNameModule',
	),
	...
),

```

# Статусы и типы

статусы по умолчанию в моделе

```
const STATUS_NOT = 0;
const STATUS_YES = 1;
```

как добавить свои типы:

```
const TYPE_GENERAL = 1;
const TYPE_LIST = 2;

public static function getTypes()
{
	return array(
		self::TYPE_GENERAL => 'Обычная страница',
		self::TYPE_LIST => 'Для списка страниц',
	);
}

public function getType()
{
	$array = self::getTypes();
	return isset($array[$this->type]) ? $array[$this->type] : null;
}
```


Yii 2 Advanced Application Template
===================================

Yii 2 Advanced Application Template is a skeleton Yii 2 application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.


DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
tests                    contains various tests for the advanced application
    codeception/         contains tests developed with Codeception PHP Testing Framework
```


REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.
