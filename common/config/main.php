<?php
return [
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'components' => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
		],
	],
	'modules' => [
		'fileProcessor' => [
			'class' => '\metalguardian\fileProcessor\Module',
			'imageSections' => [
				'configuration' => [
					'preview' => [
						'action' => 'adaptiveThumbnail',
						'width' => 100,
						'height' => 100,
					],
				],
				'banner' => [
					'standard' => [
						'action' => 'adaptiveThumbnail',
						'width' => 400,
						'height' => 300,
					],
				],
			]
		],
	],
	'bootstrap' => ['fileProcessor'],
];
