<?php

namespace configuration;

class ConfigurationModule extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
