<?php

namespace configuration\models;

use Yii;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use yii\helpers\ArrayHelper;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\Html;

/**
 * This is the model class for table "configuration".
 *
 * @property integer $id
 * @property string $config_key
 * @property string $value
 * @property string $description
 * @property integer $type
 * @property integer $preload
 */
class Configuration extends \yii\db\ActiveRecord
{
	/**
	 *
	 */
	const TYPE_INTEGER = 0;
	/**
	 *
	 */
	const TYPE_FLOAT = 1;
	/**
	 *
	 */
	const TYPE_STRING = 2;
	/**
	 *
	 */
	const TYPE_TEXT = 3;
	/**
	 *
	 */
	const TYPE_HTML = 4;
	/**
	 *
	 */
	const TYPE_FILE = 5;
	/**
	 *
	 */
	const TYPE_IMAGE = 6;
	/**
	 *
	 */
	const TYPE_BOOLEAN = 7;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'configuration';
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'config_key' => 'Ключ',
			'value' => 'Значение',
			'description' => 'Описание',
			'type' => 'Тип',
			'preload' => 'Предзагрузка',
		];
	}

}
