$(function () {
	$('.metismenu').metisMenu();

	/**
	 * Получаем переменные из адресной строки
	 * @returns {Array}
	 */
	function getUrlVars() {
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}


	// Minimalize menu when screen is less than 768px
	$(window).bind("resize", function () {
		if ($(this).width() < 769) {
			$('body').addClass('body-small')
		} else {
			$('body').removeClass('body-small')
		}
	});

	// Minimalize menu
	$('.navbar-minimalize').click(function () {
		$("body").toggleClass("mini-navbar");
		SmoothlyMenu();
	});

	/**
	 * Анимация меню
	 * @constructor
	 */
	function SmoothlyMenu() {
		if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
			// Hide menu in order to smoothly turn on when maximize menu
			$('#side-menu').hide();
			// For smoothly turn on menu
			setTimeout(
				function () {
					$('#side-menu').fadeIn(500);
				}, 100);
		} else if ($('body').hasClass('fixed-sidebar')) {
			$('#side-menu').hide();
			setTimeout(
				function () {
					$('#side-menu').fadeIn(500);
				}, 300);
		} else {
			// Remove all inline style from jquery fadeIn function to reset menu state
			$('#side-menu').removeAttr('style');
		}
	}

	// Full height of sidebar
	function fix_height() {
		var heightWithoutNavbar = $("body > #wrapper").height() - 61;
		//$(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

		var navbarHeigh = $('nav.navbar-default').height();
		var wrapperHeigh = $('#page-wrapper').height();

		if (navbarHeigh > wrapperHeigh) {
			$('#page-wrapper').css("min-height", navbarHeigh + "px");
		}

		if (navbarHeigh < wrapperHeigh) {
			$('#page-wrapper').css("min-height", $(window).height() + "px");
		}

		if ($('body').hasClass('fixed-nav')) {
			$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
		}

	}

	fix_height();

	$(window).bind("load resize scroll", function () {
		if (!$("body").hasClass('body-small')) {
			fix_height();
		}
	});

	function insertParam(key, value) {
		key = encodeURI(key);
		value = encodeURI(value);

		var kvp = document.location.search.substr(1).split('&');

		var i = kvp.length;
		var x;
		while (i--) {
			x = kvp[i].split('=');

			if (x[0] == key) {
				x[1] = value;
				kvp[i] = x.join('=');
				break;
			}
		}

		if (i < 0) {
			kvp[kvp.length] = [key, value].join('=');
		}

		//this will reload the page, it's likely better to store this until finished
		document.location.search = kvp.join('&');
	}

	function removeParam(key) {
		key = encodeURI(key);

		var kvp = document.location.search.substr(1).split('&');

		var i = kvp.length;
		var x;
		while (i--) {
			x = kvp[i].split('=');

			if (x[0] == key) {
				kvp.splice(i, 1);
				break;
			}
		}

		if (i < 0) {
			kvp[kvp.length] = [key, value].join('=');
		}

		//this will reload the pakge, it's likely better to store this until finished
		document.location.search = kvp.join('&');
	}

});
