<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="margin: 0 auto;width: 720px">
	<h1 class="logo-name">Test Test</h1>
</div>
<div class="middle-box text-center loginscreen animated fadeInDown">
	<div>
		<h3><?= Html::encode($this->title) ?></h3>

		<p>Login in. To see it in action.</p>
		<?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'm-t']); ?>
		<?= $form->field($model, 'username') ?>
		<?= $form->field($model, 'password')->passwordInput() ?>
		<?= $form->field($model, 'rememberMe')->checkbox() ?>
		<div class="form-group">
			<?= Html::submitButton('Login', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>
