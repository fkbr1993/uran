<?php
use stat\models\History;
use user\models\User;
use review\models\Review;
use comment\models\Comment;
use order\models\Order;
use stat\models\Stat;
use like\models\Like;
?>

<div class="sidebard-panel">
	<div>
		<h4>Активность за сутки</h4>
		<div class="feed-element">
			<span class="pull-left">
				Просмотры
			</span>
			<div class="media-body">
				<span class="badge badge-white">0</span>
			</div>
		</div>
	</div>

	<div class="m-t-md">
		<div class="feed-element">
			<span class="pull-left">
				Отзывы
			</span>
			<div class="media-body">
				<span class="badge badge-white">0</span>
			</div>
		</div>
		<div class="feed-element">
			<span class="pull-left">
				Комментарии
			</span>
			<div class="media-body">
				<span class="badge badge-white">0</span>
			</div>
		</div>
		<div class="feed-element">
			<span class="pull-left">
				Лайки
			</span>
			<div class="media-body">
				<span class="badge badge-white">0</span>
			</div>
		</div>
	</div>
</div>
