<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\Helper;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="pace-done">
<?php $this->beginBody() ?>
<div id="wrapper">
	<nav role="navigation" class="navbar-default navbar-static-side">
		<div class="sidebar-collapse">
			<ul id="side-menu" class="nav metismenu">
				<li class="nav-header">
					<div class="dropdown profile-element"> <span>
                            <img src="/css/img/profile_small.jpg" class="img-circle" alt="image"></span>
						<a href="#">
                            <span class="clear">
	                            <span class="block m-t-xs">
		                            <strong class="font-bold">Admin</strong>
                                </span>
	                            <span class="text-muted text-xs block">Пользователь</span>
                            </span>
						</a>
					</div>
					<div class="logo-element">
						Pr+
					</div>
				</li>
				<?php
				$menu = [
					'' => [
						'icon' => 'fa fa-th-large',
						'title' => 'Главная'
					],
					'configuration/configuration/index' => [
						'icon' => 'fa fa-gear',
						'title' => 'Настройки',
					],
					'#2' => [
						'icon' => 'fa fa-comments-o',
						'title' => 'Title',
						'items' => [
							'Sub Title 1' => ['index'],
							'Sub Title 2' => ['index'],
							'Sub Title 3' => ['index', 'deleted' => 1],
						],
					],
				];

				foreach ($menu as $k => $item) {
					$active = '';
					if (!isset($item['items']) && Yii::$app->controller->route == $k) {
						$active = ' class="active"';
					} elseif (isset($item['items']) && Helper::in_array_r(
							'/' . Yii::$app->controller->route,
							$item['items'],
							true
						)
					) {
						$active = ' class="active"';
					}
					?>
					<li<?= $active ?>>
						<?php
						if (!isset($item['items'])) {
							$url = Url::toRoute('/' . $k);
						} else {
							$url = '#';
						} ?>
						<a href="<?= $url ?>">
							<i class="<?= $item['icon'] ?>"></i>
							<span class="nav-label"><?= $item['title'] ?></span>
							<?php if (isset($item['items'])) { ?>
								<span class="fa arrow"></span>
							<?php } ?>
						</a>
						<?php if (isset($item['items'])) { ?>
							<ul class="nav nav-second-level collapse">
								<?php
								foreach ($item['items'] as $key => $i) { ?>
									<li>
										<a href="<?= Url::toRoute($i) ?>"><?= $key ?></a>
										<span class="label label-info pull-right">0</span>
									</li>
								<?php } ?>
							</ul>
						<?php } ?>
					</li>
				<?php } ?>
			</ul>
		</div>
	</nav>
	<div id="page-wrapper" class="gray-bg<?= Yii::$app->controller->action->uniqueId == 'site/index'
		? ' sidebar-content' : '' ?>">
		<div class="row border-bottom">
			<nav style="margin-bottom: 0" role="navigation" class="navbar navbar-static-top">
				<div class="navbar-header">
					<a href="#" class="navbar-minimalize minimalize-styl-2 btn btn-primary ">
						<i class="fa fa-bars"></i>
					</a>
					<?php
					//					$form = ActiveForm::begin(
					//						[
					//							'method' => 'get',
					//							'action' => '/user/user/index',
					//							'options' => ['class' => 'navbar-form-custom'],
					//						]
					//					);
					//					echo $form->field(new \user\models\UserSearch(), 'username', ['template' => '{input}'])->input(
					//						'text',
					//						['placeholder' => 'ID или Email пользователя']
					//					)->label(false);
					//					$form->end();
					?>
				</div>
				<ul class="nav navbar-top-links navbar-right">
					<li>
						<a href="#" class="count-info">
							<i class="fa fa-truck"></i>
							<span class="label label-priendifmary">0</span>
						</a>
					</li>
					<li>
						<a href="#" class="count-info" aria-expanded="false">
							<i class="fa fa-envelope"></i>
							<span class="label label-warning">0</span>
						</a>
					</li>
					<li>
						<a data-method="post" href="<?= Url::toRoute(['/site/logout']) ?>">
							<i class="fa fa-sign-out"></i>
							Выход
						</a>
					</li>
				</ul>
			</nav>
		</div>
		<?php
		$breadcrumbs = isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [];
		if ($breadcrumbs) :?>
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-9">
					<h2><?= end($breadcrumbs) ?></h2>
					<?= Breadcrumbs::widget(
						[
							'links' => $breadcrumbs,
						]
					) ?>
				</div>
			</div>
		<?php endif ?>
		<?php if (Yii::$app->controller->action->uniqueId == 'site/index') { ?>
			<?= $this->render('@backend/views/site/_sidebarPanel') ?>
		<?php } ?>
		<div class="wrapper wrapper-content">
			<?= $content ?>
		</div>
	</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
?>
