<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MetisMenuAsset extends AssetBundle
{

	public $sourcePath = '@vendor/onokumus/metismenu/dist';

	public $depends = [
		'yii\web\JqueryAsset'
	];

	public function init()
	{
		$postfix = YII_DEBUG ? '' : '.min';
		$this->js[] = 'metisMenu' . $postfix . '.js';
		$this->css[] = 'metisMenu' . $postfix . '.css';
		parent::init();
	}
}
