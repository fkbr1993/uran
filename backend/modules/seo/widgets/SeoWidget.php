<?php
namespace backend\modules\seo\widgets;

use backend\modules\seo\models\Seo;
use yii\base\Widget;

/**
 * Class SeoWidget
 *
 * @property \yii\base\Model $model
 * @property boolean $form
 */
class SeoWidget extends Widget
{

	public $model = null;

	public $form = false;

	public function run()
	{
		$seo = new Seo();
		if ($this->model) {
			if ($this->form) {
				$seo = Seo::findOne(['model_name' => Seo::getModelName($this->model), 'model_id' => 0]);

				if(!$seo) {
					$seo = new Seo();
				}

				$seo->model_name = Seo::getModelName($this->model);
				$seo->model_id = 0;
			} elseif($this->model && !$this->model->isNewRecord) {
				$seo = Seo::findOne(['model_name' => Seo::getModelName($this->model), 'model_id' => $this->model->id]);
				if(!$seo) {
					$seo = new Seo();
				}
			}
		}

		return $this->render('seo', ['seo' => $seo, 'model' => $this->model, 'isForm' => $this->form]);
	}
}

?>
