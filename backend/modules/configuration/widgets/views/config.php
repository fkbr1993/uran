<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use configuration\models\Configuration;

/* @var $this yii\web\View */
/* @var $model configuration\models\Configuration */
/* @var $form yii\widgets\ActiveForm */

$config = new Configuration();
$reflect = new \ReflectionClass($config);
$name = $reflect->getShortName();
$id = 'configuration-value';
$options = ['id' => $id, 'class' => 'form-control'];
?>
<div class="form-group field-configuration-value">
	<?php
	switch ($type) {
		case Configuration::TYPE_FILE:
			echo Html::fileInput($name . '[image_id]', $value);
			break;
		case Configuration::TYPE_IMAGE:
			echo Html::fileInput($name . '[image_id]', $value);
			break;
		case Configuration::TYPE_BOOLEAN:
			echo Html::hiddenInput($name . '[value]', 0),
			Html::beginTag('label', ['for' => 'configuration-checkbox']),
			Html::checkbox($name . '[value]', $value, ['value' => $value ? 1 : 0, 'id' => 'configuration-checkbox']), ' ' . $config->getAttributeLabel('value'),
			Html::endTag('label');
			break;
		case Configuration::TYPE_FLOAT:
		case Configuration::TYPE_STRING:
		case Configuration::TYPE_INTEGER:
			echo Html::label($config->getAttributeLabel('value'), $id, ['class' => 'control-label']),
			Html::input('text', $name . '[value]', $value, $options);
			break;
		case Configuration::TYPE_TEXT:
			echo Html::label($config->getAttributeLabel('value'), $id, ['class' => 'control-label']),
			Html::textarea($name . '[value]', $value, $options);
			break;
		case Configuration::TYPE_HTML:
			$options['id'] = 'imperavi';
			echo Html::label($config->getAttributeLabel('value'), $id, ['class' => 'control-label']),
			Html::textarea($name . '[value]', $value, $options);
			break;
		default:
			break;
	}
	?>
</div>




