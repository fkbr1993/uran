<?php
namespace configuration\widgets;

use configuration\models\Configuration;
use yii\base\Widget;

/**
 * Class SeoWidget
 *
 * @property \yii\base\Model $model
 * @property boolean $form
 */
class ConfigWidget extends Widget
{

	public $type = 0;
	public $value = false;
	public $model = null;

	public function run()
	{
		return $this->render('config', ['type' => $this->type, 'value' => $this->value]);
	}
}

?>
