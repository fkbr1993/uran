<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace configuration\assets;

use vova07\imperavi\Asset;
use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ImperAsset extends Asset
{
	public $sourcePath = '@vendor/vova07/yii2-imperavi-widget/src/assets';

	public $css = [
		'redactor.css',
	];

	/**
	 * @inheritdoc
	 */
	public $js = [
		'redactor.min.js',
		'lang/ru.js',
		'plugins/fullscreen/fullscreen.js',
		'plugins/table/table.js',
		'plugins/fontsize/fontsize.js',
		'plugins/fontcolor/fontcolor.js',
		'plugins/fontfamily/fontfamily.js'
	];

	/**
	 * @inheritdoc
	 */
	public $depends = [
		'yii\web\JqueryAsset'
	];
}
