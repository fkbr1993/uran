$(function () {
	var options = {
		'lang': 'ru',
		'minHeight': 200,
		'plugins': [
			'clips',
			'table',
			'fullscreen',
			'fontsize',
			'fontfamily',
			'fontcolor'
		]
	};

	$('#imperavi').redactor(options);

	$('#configuration-type').on('change', function () {
		$.get('/configuration/configuration/type', {id: $(this).val()}, function (r) {
			$('.field-configuration-value').replaceWith(r);
			if (r.indexOf('id="imperavi"')) {
				$('#imperavi').redactor(options);
			}
		});
	});

	$(document).on('change', '#configuration-checkbox', function() {
		var $check = $(this);
		if ($check.is(':checked')) {
			$check.val(1);
		} else {
			$check.val(0)
		}
	});

});

