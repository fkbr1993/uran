<?php

namespace configuration\components;

use configuration\models\Configuration;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\base\Component;

/**
 * Class ConfigComponent
 *
 * @property \configuration\models\Configuration $preload
 */
class ConfigComponent extends Component
{

	private $preload = null;

	public function init()
	{
		$this->preload = Configuration::findAll(['preload' => 1]);
	}

	public function get($key, $module = null, $size = null)
	{
		$value = null;

		foreach ($this->preload as $p) {
			if ($p->config_key == $key) {
				$value = $this->checkType($p, $module, $size);
				break;
			}
		}

		if (!$value) {
			$config = Configuration::findOne(['config_key' => $key]);
			if ($config) {
				$value = $this->checkType($config, $module, $size);
			}
		}

		return $value;
	}

	private function checkType($model, $module = null, $size = null)
	{
		$value = '';

		if ($model->type == Configuration::TYPE_FILE) {
			$value = FPM::originalSrc($model->image_id);
		} elseif ($model->type == Configuration::TYPE_IMAGE) {
			if ($module && $size) {
				$value = FPM::src($model->image_id, $module, $size);
			} else {
				$value = FPM::originalSrc($model->image_id);
			}
		} else {
			$value = $model->value;
		}

		return $value;
	}
}
