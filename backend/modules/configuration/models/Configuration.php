<?php

namespace configuration\models;

use Yii;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use yii\helpers\ArrayHelper;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\Html;

/**
 * This is the model class for table "configuration".
 *
 * @property integer $id
 * @property string $config_key
 * @property string $value
 * @property string $description
 * @property integer $type
 * @property integer $preload
 */
class Configuration extends \yii\db\ActiveRecord
{
	const TYPE_INTEGER = 0;
	const TYPE_FLOAT = 1;
	const TYPE_STRING = 2;
	const TYPE_TEXT = 3;
	const TYPE_HTML = 4;
	const TYPE_FILE = 5;
	const TYPE_IMAGE = 6;
	const TYPE_BOOLEAN = 7;

	public $oldType = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'configuration';
	}

	public function behaviors()
	{
		//var_dump($this->owner->scenario);exit;
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'image' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'image_id',
					'image' => false,
					'required' => false,
				]
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->image_id);

		return true;
	}

	public function beforeSave() {
		$model = Configuration::findOne($this->id);
		if (!$this->isNewRecord && ($model->type == self::TYPE_IMAGE || $model->type == self::TYPE_FILE)) {
			FPM::deleteFile($this->image_id);
			$this->image_id = null;
		}
		return true;
	}

	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			$this->setRules();

			return true;
		}

		return false;
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['config_key'], 'required'],
			[['description'], 'string'],
			[['type', 'preload'], 'integer'],
			[['config_key'], 'string', 'max' => 255],
			['value', 'required', 'except' => ['file', 'image']],
			['value', 'integer', 'on' => 'integer'],
			['value', 'number', 'on' => 'float'],
			['value', 'safe', 'on' => 'safe'],
			['image_id', 'integer', 'on' => ['file', 'image']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'config_key' => 'Ключ',
			'value' => 'Значение',
			'description' => 'Описание',
			'type' => 'Тип',
			'preload' => 'Предзагрузка',
		];
	}

	protected function setRules()
	{
		switch ($this->type) {
			case self::TYPE_FILE:
				$this->setScenario('file');
				break;
			case self::TYPE_IMAGE:
				$this->setScenario('image');
				break;
			case self::TYPE_BOOLEAN:
			case self::TYPE_INTEGER:
				$this->setScenario('integer');
				break;
			case self::TYPE_FLOAT:
				$this->setScenario('float');
				break;
			case self::TYPE_TEXT:
			case self::TYPE_HTML:
			case self::TYPE_STRING:
				$this->setScenario('safe');
				break;
			default:
				break;
		}
	}

	public static function getTypes()
	{
		return [
			self::TYPE_INTEGER => 'Целое число',
			self::TYPE_FLOAT => 'Вещественное число',
			self::TYPE_STRING => 'Строка',
			self::TYPE_TEXT => 'Текст',
			self::TYPE_HTML => 'HTML',
			self::TYPE_FILE => 'Файл',
			self::TYPE_IMAGE => 'Изображение',
			self::TYPE_BOOLEAN => 'Логический'
		];
	}

	public static function getValue($model, $module = null, $size = null) {
		$str = '';
		switch ($model->type) {
			case Configuration::TYPE_FILE:
				$file = FPM::originalSrc($model->image_id);
				$str = Html::a($file,$file);
				break;
			case Configuration::TYPE_IMAGE:
				if ($module && $size) {
					$file = FPM::src($model->image_id, $module, $size);
				} else {
					$file = FPM::originalSrc($model->image_id);
				}
				$str = Html::img($file);
				break;
			case Configuration::TYPE_BOOLEAN:
			case Configuration::TYPE_FLOAT:
			case Configuration::TYPE_STRING:
			case Configuration::TYPE_INTEGER:
			case Configuration::TYPE_TEXT:
			case Configuration::TYPE_HTML:
				$str = $model->value;
				break;
			default:
				break;
		}
		return $str;
	}
}
