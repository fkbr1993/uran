<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model configuration\models\Configuration */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Параметры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5><?= Html::encode($this->title) ?></h5>
				<div class="ibox-tools">
					<?= Html::a(
						'Редактировать',
						['update', 'id' => $model->id],
						['class' => 'btn btn-primary btn-xs']
					) ?>
					<?= Html::a(
						'Удалить',
						['delete', 'id' => $model->id],
						[
							'class' => 'btn btn-danger btn-xs',
							'data' => [
								'confirm' => 'Вы уверены, что хотите удалить?',
								'method' => 'post',
							],
						]
					) ?>
				</div>
			</div>
			<div class="ibox-content">
				<?= DetailView::widget(
					[
						'model' => $model,
						'attributes' => [
							'id',
							'config_key',
							[
								'attribute' => 'value',
								'format' => 'raw',
								'value' => \configuration\models\Configuration::getValue($model),
							],
							'description:ntext',
							'type',
							'preload',
						],
					]
				) ?>
			</div>
		</div>
	</div>
</div>
