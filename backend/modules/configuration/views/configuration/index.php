<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel configuration\models\ConfigurationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Параметры';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Всего параметров: <?= $dataProvider->totalCount ?></h5>
				<div class="ibox-tools">
					<?= Html::a('Новый параметр', ['create'], ['class' => 'btn btn-success btn-xs']) ?>
				</div>
			</div>
			<div class="ibox-content">

				<?= GridView::widget(
					[
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'summary' => '',
						'options' => ['class' => 'table-responsive'],
						'tableOptions' => ['class' => 'footable table table-stripped'],
						'pager' => [
							'options' => ['class' => 'pagination pull-right'],
						],
						'columns' => [
							'id',
							'config_key',
							[
								'attribute' => 'value',
								'format' => 'raw',
								'value' => function ($model) {
									return \configuration\models\Configuration::getValue(
										$model,
										'configuration',
										'preview'
									);
								},
							],
							'description:ntext',
							'type',
							// 'preload',

							[
								'contentOptions' => ['class' => 'col-md-1 text-right'],
								'class' => 'backend\components\SmallActionColumn'
							],
						],
					]
				); ?>

			</div>
		</div>
	</div>
</div>
