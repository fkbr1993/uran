<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model configuration\models\Configuration */

$this->title = 'Новый параметр';
$this->params['breadcrumbs'][] = ['label' => 'Параметры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5><?= Html::encode($this->title) ?></h5>
			</div>
			<div class="ibox-content configuration-create">
				<?= $this->render(
					'_form',
					[
						'model' => $model,
					]
				) ?>
			</div>
		</div>
	</div>
</div>
