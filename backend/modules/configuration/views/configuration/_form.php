<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use configuration\models\Configuration;

/* @var $this yii\web\View */
/* @var $model configuration\models\Configuration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="configuration-form">

	<?php $form = ActiveForm::begin(
		[
			'options' => [
				'enctype' => 'multipart/form-data',
			],
		]
	); ?>

	<?= $form->field($model, 'config_key')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'type')->dropDownList(Configuration::getTypes()) ?>

	<?= \configuration\widgets\ConfigWidget::widget(
		['type' => $model->type, 'value' => $model->value]
	) ?>

	<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'preload')->checkbox() ?>

	<div class="form-group">
		<?= Html::submitButton(
			$model->isNewRecord ? 'Создать' : 'Сохранить',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
		) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
<?php
\configuration\assets\ConfigAsset::register($this);
\configuration\assets\ImperAsset::register($this);
?>
