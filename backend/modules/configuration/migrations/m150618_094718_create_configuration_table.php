<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_094718_create_configuration_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%configuration}}', [
			'id' => Schema::TYPE_PK,
			'config_key' => Schema::TYPE_STRING . ' NOT NULL',
			'value' => Schema::TYPE_TEXT,
			'description' => Schema::TYPE_TEXT,
			'type' => Schema::TYPE_SMALLINT,
			'preload' => Schema::TYPE_BOOLEAN . ' DEFAULT 0 NOT NULL',
			'image_id' => Schema::TYPE_INTEGER,
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%configuration}}');
	}

}
