<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150410_114026_create_configuration_table
 */
class m150410_114026_create_configuration_table extends Migration
{
	public $tableName = '{{%configuration}}';

    public function up()
    {
	    $this->createTable(
		    $this->tableName,
		    [
			    'id' => Schema::TYPE_INTEGER. ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
			    'config_key' => Schema::TYPE_STRING. ' NOT NULL COMMENT "Ключ"',
			    'value' =>  Schema::TYPE_TEXT. ' DEFAULT NULL COMMENT "Значение"',
			    'description' => Schema::TYPE_STRING. ' NOT NULL COMMENT "Описание"',
			    'type' => Schema::TYPE_SMALLINT. '(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT "Тип"',
			    'created' => Schema::TYPE_DATETIME. ' NOT NULL COMMENT "Создано"',
			    'modified' => Schema::TYPE_DATETIME. ' NOT NULL COMMENT "Обновлено"',
			    'preload' => Schema::TYPE_BOOLEAN . ' DEFAULT 0 COMMENT "Предзагрузка"',
			    'image_id' => Schema::TYPE_INTEGER,
		    ],
		    'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
	    );
	    $this->createIndex('unique_config_key', $this->tableName, 'config_key', true);
    }

    public function down()
    {
	    $this->dropTable($this->tableName);
    }
}
